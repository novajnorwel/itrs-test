ITRS-TEST

Remarks
=======
- The database connection details must be set in config/global.php.
- The database dump file can be found in 'itrs-test.sql'.
- The root directory is 'html', so the virtual host must point to it.

Any question/problem, please contact novajnorwel@gmail.com
