<?php
return array(
	'view' => array(
		'path'       => APP_DIR . DS . 'view',
		'layout'     => 'default',
		'layoutPath' => APP_DIR . DS. 'view' . DS . 'layout',
	),
	'route' => array(
		'/' => array('controller' => 'index', 'action' => 'index'),
	),
	'db' => array(
		'driver'   => 'mysql',
		'host'     => 'localhost',
		'database' => 'itrs-test',
		'user'     => 'itrs-test',
		'password' => 'itrs-test',
		'encoding' => 'UTF8',
	)
);