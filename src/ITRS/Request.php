<?php

namespace ITRS;

/**
 * Request class
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Request {
	protected $_request = array();
	
	public function __construct() {
		$this->_setRequest(filter_input_array(INPUT_SERVER));
	}
	
	public function getPost() {
		return filter_input_array(INPUT_POST);
	}
	
	public function isGet() {		
		if(isset($this->_request['REQUEST_METHOD'])) {
			return $this->_request['REQUEST_METHOD'] === 'GET';
		} else {
			return false;
		}
	}
	
	public function isPost() {		
		if(isset($this->_request['REQUEST_METHOD'])) {
			return $this->_request['REQUEST_METHOD'] === 'POST';
		} else {
			return false;
		}
	}
	
	//Getters and setters
	protected function _setRequest(array $request) {
		$this->_request = $request;
		return $this;
	}
}