<?php

namespace ITRS\Mapper;

/**
 * Mapper with common properties and methods for all mappers
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class AbstractMapper {
	protected static $_db;
	
	public function __construct() {
		if(!self::$_db) {
			self::$_db = \ITRS\Database::init();
		}
	}
	
	/**
	 * @return \ITRS\Database
	 */
	protected function _getDatabaseConnection() {
		return self::$_db;
	}
}