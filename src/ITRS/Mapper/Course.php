<?php

namespace ITRS\Mapper;

/**
 * Course mapper
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Course extends AbstractMapper {
	/**
	 * 
	 * @return \ITRS\Model\Course[]
	 */
	public function getCourses($limit = 0) {
		$db     = $this->_getDatabaseConnection();
		$sql    = "
			SELECT
				id,
				name,
				description,
				image,
				priority
			FROM
				available_courses
			ORDER BY
				priority ASC,
				name     ASC
			-- LIMIT
			%s
		";
		$limitSql   = $limit !== 0 ? 'LIMIT ' . $limit : '';
		$sql        = sprintf($sql, $limitSql);
		$rows       = $db->query($sql);
		$courseList = array();
		
		foreach( $rows as $course ) {
			$courseList[] = new \ITRS\Model\Course($course);
		}
		
		return $courseList;
	}
}