<?php

namespace ITRS\Mapper;

/**
 * Contact mapper
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Contact extends AbstractMapper {
	/**
	 * @param \ITRS\Model\Contact $contact
	 * @return bool
	 */
	public function addContact($contact)
	{
		$db     = $this->_getDatabaseConnection();
		$sql    = "
			INSERT INTO contacts (
				name,
				email,
				subject,
				`comment`
			) VALUES (
				:name,
				:email,
				:subject,
				:comment
			)
		";
		$bindParams = array(
			':name'    => $contact->getName(),
			':email'   => $contact->getEmail(),
			':subject' => $contact->getSubject(),
			':comment' => $contact->getComment(),
		);
		$result = $db->query($sql, $bindParams);
		
		return is_array($result);
	}
}