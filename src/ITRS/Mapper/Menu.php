<?php

namespace ITRS\Mapper;

/**
 * Menu mapper
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Menu extends AbstractMapper {
	/**
	 * 
	 * @return \ITRS\Model\Menu[]
	 */
	public function getMenus() {
		$db     = $this->_getDatabaseConnection();
		$sql    = "
			SELECT
				id,
				label,
				url,
				priority
			FROM
				menu_items
			ORDER BY
				priority ASC,
				label    ASC
		";
		$rows     = $db->query($sql);
		$menuList = array();
		
		foreach( $rows as $menu ) {
			$menuList[] = new \ITRS\Model\Menu( $menu );
		}
		
		return $menuList;
	}
}