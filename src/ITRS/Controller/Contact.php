<?php

namespace ITRS\Controller;

/**
 * Contact controller
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Contact extends AbstractController {
	public function index()
	{
		$contact = new \ITRS\Model\Contact();
		$request = $this->getRequest();
		
		if($request->isPost()) {
			$contactService = new \ITRS\Service\Contact();
			$contact        = new \ITRS\Model\Contact($request->getPost());
			
			if($contactService->validateContact($contact)) {
				if($contactService->addContact($contact)) {
					\ITRS\Messenger::addMessage(\ITRS\Messenger::SUCCESS, _('Your request has been successfully sent.'));
					return $this->redirect('/contact');
				} else {
					\ITRS\Messenger::addMessage(\ITRS\Messenger::ERROR, _('Ooops! Your request could not be sent. Please try again later.'));
				}
			}
		}
		
		return compact('contact');
	}
}