<?php

namespace ITRS\Controller;

/**
 * Controller with common properties and methods for all controllers
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class AbstractController {
	protected static $_request;
	
	public function __construct() {
		if(!self::$_request) {
			self::$_request = new \ITRS\Request();
		}
	}
	
	public function preAction()
	{
		$menuService = new \ITRS\Service\Menu();
		
		//Prepare view
		$menuList = $menuService->getMenus();
		
		return compact('menuList');
	}
	
	public function postAction()
	{
		return array();
	}
	
	public function redirect($url)
	{
		header('Location: ' . $url);
	}
	
	//Getters and setters

	/**
	 * @return \ITRS\Request
	 */
	public function getRequest() {
		return self::$_request;
	}
}