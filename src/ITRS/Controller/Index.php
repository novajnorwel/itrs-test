<?php

namespace ITRS\Controller;

/**
 * Index controller
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Index extends AbstractController {
	public function index() {
		$courseService = new \ITRS\Service\Course();
		
		//Prepare view
		$courseList = $courseService->getCourses(3);

		return compact('courseList');
	}
}