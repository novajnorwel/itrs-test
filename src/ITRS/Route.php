<?php

namespace ITRS;

/**
 * Router
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Route {
	protected static $_action        = '';
	protected static $_controller    = '';
	protected static $_defaultAction = 'index';
	protected static $_routes        = array();
	
	public static function addRoutes(array $routes) {
		self::$_routes = array_merge(self::$_routes, $routes);
	}
	
	public static function getRoute($route) {
		$formattedRoute = '/' . $route;
		
		if(isset(self::$_routes[$formattedRoute])) {
			return self::$_routes[$formattedRoute];
		} else {
			//Search for controller/action
			$parsedRoute = explode('/', $route);
			$result      = array(
				'controller' => '',
				'action'     => self::$_defaultAction,
			);
			
			if(isset($parsedRoute[0])) {
				$result['controller'] = $parsedRoute[0];
			}
			
			if(isset($parsedRoute[1])) {
				$result['action'] = $parsedRoute[1];
			}
			
			return $result;
		}
	}
	
	public static function init() {
		$config = Application::getConfig();
		
		if(isset($config['route'])) {
			self::addRoutes($config['route']);
		}
	}
	
	/**
	 * Manage friendly urls
	 * 
	 * @return \ITRS\View\View
	 * @throws \Exception
	 */
	public static function parseUri() {
		$uri = filter_input_array(INPUT_GET, array(
			'request' => FILTER_SANITIZE_URL
		));

		if(!$uri['request']) {
			$uri = array('request' => '');
		} else {
			$uri['request'] = rtrim($uri['request'], '/');
		}
		
		if(($route = self::getRoute($uri['request']))) {
			self::setController($route['controller']);
			self::setAction($route['action']);
		} else {
			http_response_code(404);
			throw new \Exception(_('Page not found'));
		}
	}
	
	public static function getAction() {
		return self::$_action;
	}
	
	protected static function setAction($action) {
		self::$_action = $action;
		return self::class;
	}
	
	public static function getDefaultAction() {
		return self::$_defaultAction;
	}
	
	protected static function setDefaultAction($action) {
		self::$_defaultAction = $action;
		return self::class;
	}
	
	public static function getController() {
		return self::$_controller;
	}
	
	protected static function setController($controller) {
		self::$_controller = $controller;
		return self::class;
	}
}