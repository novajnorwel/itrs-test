<?php

namespace ITRS;

/**
 * Application bootstrap
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Application {
	protected static $_config = array();
	
	public static function init() {
		session_start();
		self::_loadConfig();
		Route::init();
		Route::parseUri();
		
		//Get controller and action
		$controllerName = Route::getController();
		$action         = Route::getAction();
		$namespace      = '\\ITRS\\Controller\\' . ucfirst($controllerName);
		$controller     = new $namespace();
		
		//Call controller's action
		$preAction = $controller->preAction();
		
		if(method_exists($controller, $action)) {
			$currentAction = $controller->$action();
		} else {
			throw new \Exception('Action not found');
		}
		
		$postAction = $controller->postAction();
		
		$view = new \ITRS\View\View(array_merge($preAction, $currentAction, $postAction));

		$view->setController($controllerName);
		$view->setAction($action);
		
		echo $view->render();
	}
	
	protected static function _loadConfig() {
		$defaultConfig = array(
			'view' => array(
				'path'       => '',
				'layout'     => '',
				'layoutPath' => '',
			),
		);
		
		$config = include APP_DIR . DS . 'config' . DS . 'global.php';
		
		self::setConfig(array_merge($defaultConfig, $config));
	}
	
	public static function getConfig() {
		return self::$_config;
	}
	
	protected static function setConfig(array $config = array()) {
		self::$_config = $config;
	}
}