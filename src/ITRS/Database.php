<?php

namespace ITRS;

/**
 * Database manager
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Database {
	protected static $_db;
	protected static $_defaultDbConfig = array(
		'driver'   => '',
		'host'     => '',
		'database' => '',
		'user'     => '',
		'password' => '',
		'encoding' => '',
	);
	
	protected $_conn;
	
	private function __construct() {
		$config = Application::getConfig();
		
		if(isset($config['db'])) {
			$dbConfig = array_merge( self::$_defaultDbConfig , $config['db']);
			
			try {
				$dsn         = sprintf('%s:host=%s;dbname=%s', $dbConfig['driver'], $dbConfig['host'], $dbConfig['database']);
				$this->_conn = new \PDO($dsn, $dbConfig['user'], $dbConfig['password'], array(
					\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '{$dbConfig['encoding']}'"
				));
				
			} catch(\Exception $e) {
				throw new \Exception(_('Connection failed: ' . $e->getMessage()));
			}
		} else {
			throw new \Exception(_('Database configuration not found'));
		}
	}
	
	/**
	 * @return \ITRS\Database
	 */
	public static function init() {
		if(!self::$_db) {
			self::$_db = new self();
		}
		
		return self::$_db;
	}
	
	public function query($sql, $params = array()) {
		$statement = $this->_conn->prepare($sql);
		
		if($statement->execute($params)) {
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}
}