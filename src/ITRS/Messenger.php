<?php

namespace ITRS;

/**
 * Messenger
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Messenger {
	const ALL     = 15;
	const ERROR   = 8;
	const WARNING = 4;
	const INFO    = 2;
	const SUCCESS = 1;
	
	protected static $_template = '<div class="alert alert-%s alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert"><span>&times;</span></button><p>%s</p></div>';
	
	public static function addMessage($type = self::ERROR, $message)
	{
		$_SESSION['messages'][$type][] = $message;
	}
	
	public static function render($type = self::ALL)
	{
		if($type & self::ERROR) {
			if(!empty($_SESSION['messages'][self::ERROR])) {
				echo sprintf(self::getTemplate(), 'danger', implode('</p><p>', $_SESSION['messages'][self::ERROR]));
				unset($_SESSION['messages'][self::ERROR]);
			}
			
			self::render($type & !self::ERROR);
		}
		
		if($type & self::WARNING) {
			if(!empty($_SESSION['messages'][self::WARNING])) {
				echo sprintf(self::getTemplate(), 'warning', implode('</p><p>', $_SESSION['messages'][self::WARNING]));
				unset($_SESSION['messages'][self::WARNING]);
			}
			
			self::render($type & !self::WARNING);
		}
		
		if($type & self::INFO) {
			if(!empty($_SESSION['messages'][self::INFO])) {
				echo sprintf(self::getTemplate(), 'info', implode('</p><p>', $_SESSION['messages'][self::INFO]));
				unset($_SESSION['messages'][self::INFO]);
			}
			
			self::render($type & !self::INFO);
		}
		
		if($type & self::SUCCESS) {
			if(!empty($_SESSION['messages'][self::SUCCESS])) {
				echo sprintf(self::getTemplate(), 'success', implode('</p><p>', $_SESSION['messages'][self::SUCCESS]));
				unset($_SESSION['messages'][self::SUCCESS]);
			}
			
			self::render($type & !self::SUCCESS);
		}
	}
	
	public static function getTemplate()
	{
		return self::$_template;
	}
	
	public static function setTemplate($template)
	{
		self::$_template = $template;
	}
}