<?php

namespace ITRS\Service;

/**
 * Course service
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Course extends AbstractService {
	public function __construct() {
		$this->_setMapper(new \ITRS\Mapper\Course());
	}
	
	public function getCourses($limit = 0) {
		$courseList = $this->getMapper()->getCourses($limit);
		
		return $courseList;
	}
	
	protected function _setMapper(\ITRS\Mapper\Course $mapper) {
		$this->_mapper = $mapper;
	}
			
	//Getters and setters
	/**
	 * @return \ITRS\Mapper\Course
	 */
	public function getMapper() {
		return $this->_mapper;
	}
}