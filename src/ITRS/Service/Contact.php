<?php

namespace ITRS\Service;

/**
 * Contact service
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Contact extends AbstractService {
	public function __construct()
	{
		$this->_setMapper(new \ITRS\Mapper\Contact());
	}
	
	protected function _setMapper(\ITRS\Mapper\Contact $mapper)
	{
		$this->_mapper = $mapper;
	}
	
	public function validateContact(\ITRS\Model\Contact &$contact)
	{
		$errors  = array();
		
		//Empty fields
		if(empty($contact->getName())) {
			$errors['name'][] = _("This field can't be left empty");
		}
		
		if(empty($contact->getEmail())) {
			$errors['email'][] = _("This field can't be left empty");
		}
		
		if(empty($contact->getSubject())) {
			$errors['subject'][] = _("This field can't be left empty");
		}
		
		//Valid email
		if(!preg_match('/[a-zA-z0-9\.\-_]{3,}@[a-zA-z0-9\-_]{2,}\.[a-zA-z]{2,}/', $contact->getEmail())) {
			$errors['email'][] = _("This doesn't look like an email");
		}
		
		$contact->setErrors($errors);
		
		return empty($errors);
	}
	
	public function addContact(\ITRS\Model\Contact $contact)
	{
		return $this->getMapper()->addContact($contact);
	}
			
	//Getters and setters
	/**
	 * @return \ITRS\Mapper\Contact
	 */
	public function getMapper() {
		return $this->_mapper;
	}
}