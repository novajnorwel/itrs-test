<?php

namespace ITRS\Service;

/**
 * Menu service
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Menu extends AbstractService {
	public function __construct() {
		$this->_setMapper(new \ITRS\Mapper\Menu());
	}
	
	public function getMenus() {
		$menuList = $this->getMapper()->getMenus();
		
		return $menuList;
	}
	
	protected function _setMapper(\ITRS\Mapper\Menu $mapper) {
		$this->_mapper = $mapper;
	}
			
	//Getters and setters
	/**
	 * @return \ITRS\Mapper\Menu
	 */
	public function getMapper() {
		return $this->_mapper;
	}
}