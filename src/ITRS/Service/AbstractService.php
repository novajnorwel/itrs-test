<?php

namespace ITRS\Service;

/**
 * Service with common properties and methods for all services
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class AbstractService {
	protected $_mapper;
}