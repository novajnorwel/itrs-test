<?php

namespace ITRS\Model;

/**
 * Model with common properties and methods for all models
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class AbstractModel
{
	protected $_errors = array();
	
	public function hasError($key)
	{
		$errors = $this->getErrors();
		return array_key_exists($key, $errors);
	}
	
	public function getError($key)
	{
		if($this->hasError($key)) {
			$errors = $this->getErrors();
			return $errors[$key];
		}
		
		return '';
	}
	
	//Getters and setters
	public function getErrors()
	{
		return $this->_errors;
	}
	
	public function setErrors($errors)
	{
		$this->_errors = $errors;
		return $this;
	}
}