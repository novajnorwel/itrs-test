<?php

namespace ITRS\Model;

/**
 * Course model
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Course extends AbstractModel {
	protected $_id;
	protected $_name;
	protected $_description;
	protected $_image;
	protected $_priority;
	
	public function __construct(array $data = array()) {
		if($data) {
			$this->setData($data);
		}
	}
	
	public function setData(array $data) {
		$this->_id          = isset($data['id']) ? $data['id'] : '';
		$this->_name        = isset($data['name']) ? $data['name'] : '';
		$this->_description = isset($data['description']) ? $data['description'] : '';
		$this->_image       = isset($data['image']) ? $data['image'] : '';
		$this->_priority    = isset($data['priority']) ? $data['priority'] : '';
	}
	
	//Getters and setters
	public function getId() {
		return $this->_id;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getDescription($limit = 0) {
		$description = $this->_description;
		
		if($limit > 0 && strlen($description) > $limit) {
			$description = substr($description, 0, $limit) . '...';
		}
		
		return $description;
	}
	
	public function getImage() {
		return $this->_image;
	}
	
	public function getPriority() {
		return $this->_priority;
	}
}