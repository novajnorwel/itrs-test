<?php

namespace ITRS\Model;

/**
 * Menu model
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Menu extends AbstractModel {
	protected $_id;
	protected $_label;
	protected $_url;
	protected $_priority;
	
	public function __construct(array $data = array()) {
		if($data) {
			$this->setData($data);
		}
	}
	
	public function setData(array $data) {
		$this->_id       = isset($data['id']) ? $data['id'] : '';
		$this->_label    = isset($data['label']) ? $data['label'] : '';
		$this->_url      = isset($data['url']) ? $data['url'] : '';
		$this->_priority = isset($data['priority']) ? $data['priority'] : '';
	}
	
	//Getters and setters
	public function getId() {
		return $this->_id;
	}
	
	public function getLabel() {
		return $this->_label;
	}
	
	public function getUrl() {
		return $this->_url;
	}
	
	public function getPriority() {
		return $this->_priority;
	}
}