<?php

namespace ITRS\Model;

/**
 * Contact model
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class Contact extends AbstractModel {
	protected $_name;
	protected $_email;
	protected $_subject;
	protected $_comment;
	
	public function __construct(array $data = array()) {
		if($data) {
			$this->setData($data);
		}
	}
	
	public function setData(array $data) {
		$this->_name    = isset($data['name']) ? $data['name'] : '';
		$this->_email   = isset($data['email']) ? $data['email'] : '';
		$this->_subject = isset($data['subject']) ? $data['subject'] : '';
		$this->_comment = isset($data['comment']) ? $data['comment'] : '';
	}
	
	//Getters and setters	
	public function getName() {
		return $this->_name;
	}
	
	public function getEmail() {
		return $this->_email;
	}
	
	public function getSubject() {
		return $this->_subject;
	}
	
	public function getComment() {
		return $this->_comment;
	}
}