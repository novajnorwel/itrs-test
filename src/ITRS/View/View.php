<?php

namespace ITRS\View;

/**
 * Class for rendering html
 *
 * @author Francisco López <francisco.lopez@ongo.es>
 */
class View implements \ArrayAccess {
	protected $_action      = '';
	protected $_content     = '';
	protected $_controller  = '';
	protected $_data        = array();
	protected $_docTitle    = null;
	protected $_layout      = '';
	protected $_headScripts = array();
	
	public function __construct(array $data = array()) {
		if($data) {
			$this->setData($data);
		}
	}
	
	public function render() {
		$controller = $this->getController();
		$action     = $this->getAction();
		$config     = \ITRS\Application::getConfig();
		
		if($controller && $action) {
			$this->setContent($config, $controller, $action);
		}
		
		$this->setLayout($config['view']['layoutPath'] . DS . $config['view']['layout'] . '.php');
		
		return $this->getLayout();
	}
	
	//Getters and setters
	public function getAction() {
		return $this->_action;
	}
	
	public function setAction($action = '') {
		$this->_action = $action;
		return $this;
	}
	
	public function getContent() {
		return $this->_content;
	}
	
	public function setContent(array $config = array(), $controller = '', $action = '') {
		ob_start();
		
		//Add variables from controller to view
		$data = $this->getData();
		
		if($data) {
			foreach($data as $var => $value) {
				$$var = $value;
			}
		}
		
		//Load the template
		include $config['view']['path'] . DS . $controller . DS . $action . '.php';
		
		$this->_content = ob_get_clean();
		
		return $this;
	}
	
	public function getController() {
		return $this->_controller;
	}
	
	public function setController($controller = '') {
		$this->_controller = $controller;
		return $this;
	}
	
	public function getData() {
		return $this->_data;
	}
	
	public function setData($data = array()) {
		$this->_data = $data;
		return $this;
	}
	
	public function getDocTitle() {
		if($this->_docTitle === null) {
			$this->setDocTitle(ucfirst($this->getController()) . ' - ' . ucfirst($this->getAction()));
		}
		
		return $this->_docTitle;
	}
	
	public function setDocTitle($title = '') {
		$this->_docTitle = $title;
		return $this;
	}
	
	public function getLayout() {
		return $this->_layout;
	}
	
	public function setLayout($layout = '') {
		if(file_exists($layout)) {
			ob_start();
			
			//Add variables from controller to view
			$data = $this->getData();

			if($data) {
				foreach($data as $var => $value) {
					$$var = $value;
				}
			}
			
			include $layout;
			
			$this->_layout = ob_get_clean();
		}
		
		return $this;
	}
	
	public function getHeadScripts()
	{
		$scriptFormat = '<script type="text/javascript" src="%s"></script>';
		$scripts      = array();
		$scriptPath   = 'js' . DS;
		
		foreach($this->_headScripts as $script) {
			$scripts[] = sprintf($scriptFormat, $scriptPath . $script);
		}
		
		return implode('\n', $scripts);
	}
	
	public function setHeadScript($fileName)
	{
		$this->_headScripts[] = $fileName;
		return $this;
	}

	public function offsetExists($offset)
	{
		return isset($this->_data[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
	}

	public function offsetSet($offset, $value)
	{
		$this->_data[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		if(isset($this->_data[$offset])) {
			unset($this->_data[$offset]);
		}
	}
}