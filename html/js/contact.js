function validateContactForm() {
	var name         = $('#name');
	var email        = $('#email');
	var subject      = $('#subject');
	var nameVal      = name.val();
	var emailVal     = email.val();
	var subjectVal   = subject.val();
	var nameValid    = true;
	var emailValid   = true;
	var subjectValid = true;
	
	nameValid = nameVal.trim().match(/.+/g) != null;
	
	if(!nameValid) {
		addError(name);
	} else {
		removeError(name);
	}
	
	emailValid = emailVal.trim().match(/[a-zA-z0-9\.\-_]{3,}@[a-zA-z0-9\-_]{2,}\.[a-zA-z]{2,}/g) != null;
	
	if(!emailValid) {
		addError(email);
	} else {
		removeError(email);
	}
	
	subjectValid = subjectVal.trim().match(/.+/g) != null;
	
	if(!subjectValid) {
		addError(subject);
	} else {
		removeError(subject);
	}
	
	return nameValid && emailValid && subjectValid;
}

function addError(input) {
	removeError(input);
	input.parent().addClass('has-error');
	input.after('<span class="help-block">' + input.data('error-message') + '</span>');
}

function removeError(input) {
	input.parent().find('.help-block').remove();
	input.parent().removeClass('has-error');
}