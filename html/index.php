<?php
//Add custom classes folder to php include path
set_include_path(get_include_path() . PATH_SEPARATOR . '.src');

//Defines
define('APP_DIR', dirname(__DIR__));
define('DS', DIRECTORY_SEPARATOR);

//Autoload classes
spl_autoload_register(function($namespace) {
	$classPath = str_replace('\\', DS, $namespace);
	require_once '..' . DS . 'src' . DS . $classPath . '.php';
});

//Init app
\ITRS\Application::init();