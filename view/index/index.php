<?php $this->setDocTitle( 'ITRS - Training'); ?>
<div class="row">
	<div class="col-md-8">
		<?php if(is_array($courseList) && count($courseList) > 0) : ?>
		<div class="section">
			<div class="section-title"><?= _('Available courses'); ?></div>
			<div class="section-body">
				<div class="row">
					<?php foreach($courseList as $course) : ?>
					<div class="col-md-4">
						<div class="course-item">
							<div class="course-image">
								<img src="<?= $course->getImage(); ?>">
							</div>
							<div class="course-name">
								<?= $course->getName(); ?>
							</div>
							<div class="course-description">
								<?= $course->getDescription(125); ?>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="col-md-4">
		<div class="sidebar">
			<div class="sidebar-item">
				<img class="img-responsive" src="img/sidebar1.jpg">
			</div>
			<div class="sidebar-item">
				<img class="img-responsive" src="img/sidebar2.jpg">
			</div>
		</div>
	</div>
</div>
