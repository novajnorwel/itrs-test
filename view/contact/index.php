<?php
	$this->setDocTitle('ITRS - Contact');
	$this->setHeadScript('contact.js');
?>
<p><?= _('Do you have any question? Please, don\'t hesitate to send us your questions filling the form below:'); ?></p>
<form id="contact-form" method="post" onsubmit="return validateContactForm();">
	<?php
		$hasError = $contact->hasError('name');
		$class    = $hasError ? ' has-error' : '';
	?>
	<div class="form-group<?= $class; ?>">
		<label class="control-label" for="name"><?= _('Name'); ?></label>
		<input id="name" class="form-control" name="name" value="<?= $contact->getName(); ?>" required autocomplete="off" data-error-message="<?= _("This field can't be left blank"); ?>">
		<?php
			if($hasError) :
				foreach($contact->getError('name') as $error) :
		?>
		<span class="help-block"><?= $error; ?></span>
		<?php
				endforeach;
			endif;
		?>
	</div>
	<?php
		$hasError = $contact->hasError('email');
		$class    = $hasError ? ' has-error' : '';
	?>
	<div class="form-group<?= $class; ?>">
		<label class="control-label" for="email"><?= _('Email'); ?></label>
		<input id="email" type="email" class="form-control" name="email" value="<?= $contact->getEmail(); ?>" required autocomplete="off" data-error-message="<?= _("This doesn't look like an email"); ?>">
		<?php
			if($hasError) :
				foreach($contact->getError('email') as $error) :
		?>
		<span class="help-block"><?= $error; ?></span>
		<?php
				endforeach;
			endif;
		?>
	</div>
	<?php
		$hasError = $contact->hasError('subject');
		$class    = $hasError ? ' has-error' : '';
	?>
	<div class="form-group<?= $class; ?>">
		<label class="control-label" for="subject"><?= _('Subject'); ?></label>
		<input id="subject" class="form-control" name="subject" value="<?= $contact->getSubject(); ?>" required autocomplete="off" data-error-message="<?= _("This field can't be left blank"); ?>">
		<?php
			if($hasError) :
				foreach($contact->getError('subject') as $error) :
		?>
		<span class="help-block"><?= $error; ?></span>
		<?php
				endforeach;
			endif;
		?>
	</div>
	<div class="form-group">
		<label class="control-label" for="comment"><?= _('Comment'); ?></label>
		<textarea id="subject" class="form-control" name="comment" autocomplete="off"><?= $contact->getComment(); ?></textarea>
	</div>
	<button class="btn btn-primary"><?= _('Submit'); ?></button>
</form>