<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?= $this->getDocTitle(); ?></title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.min.css">
		<link rel="stylesheet" type="text/css" href="css/itrs.css">
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
		<?= $this->getHeadScripts(); ?>
	</head>
	<body>
		<div class="container">
			<div class="messages">
				<?php \ITRS\Messenger::render(); ?>
			</div>
			<div class="header">
				<a href="/"><img src="img/logo_header.jpg"></a>
			</div>
			<div class="menu-container">
				<?php
					if( !empty($menuList) && is_array($menuList)) :
						foreach($menuList as $menu) :
				?>
				<div class="menu-item">
					<a href="<?= $menu->getUrl(); ?>"><?= $menu->getLabel(); ?></a>
				</div>
				<?php
						endforeach;
					endif;
				?>
			</div>
			<div class="banner"></div>
			<div class="content">
				<?= $this->getContent(); ?>
			</div>
		</div>
		<div class="footer">
			<div class="container">
				<div class="pull-right">
					<img class="img-responsive" src="img/logo_footer.jpg">
				</div>
			</div>
		</div>
	</body>
</html>
